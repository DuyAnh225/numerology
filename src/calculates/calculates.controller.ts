import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Query,
} from "@nestjs/common";
import { CalculatesService } from "./calculates.service";

@Controller("calculates")
export class CalculatesController {
  constructor(private readonly calculatesService: CalculatesService) {}

  // create(@Body() body: CreateCategoryDto): Promise<Category> {
  //   return this.categoryService.create(body);
  // }
  @Get("/life-path")
  calculateLifePath(@Body("date") date: Date): Promise<number> {
    return this.calculatesService.lifePath(date);
  }

  @Get("/mission")
  calculateMission(@Query("name") name: string): Promise<number> {
    return this.calculatesService.mission(name);
  }

  @Get("/internal")
  calculateInternal(@Query("name") name: string): Promise<number> {
    return this.calculatesService.internal(name);
  }

  @Get("/personality")
  calculatePersonality(@Query("name") name: string): Promise<number> {
    return this.calculatesService.personality(name);
  }

  @Get("/connection")
  calculateConnection(@Query("name") name: string): Promise<number> {
    return this.calculatesService.connection(name);
  }

  @Get("/balance")
  calculateBalance(@Query("name") name: string): Promise<number> {
    return this.calculatesService.balance(name);
  }

  @Get("/miss")
  calculateMiss(@Query("name") name: string): Promise<number> {
    return this.calculatesService.miss(name);
  }

  @Get("/repeat")
  calculateRepeat(@Query("name") name: string): Promise<number> {
    return this.calculatesService.repeat(name);
  }

  @Get("/logic")
  calculateLogic(@Query("name") name: string): Promise<number> {
    return this.calculatesService.logic(name);
  }

  @Get("/intuitive")
  calculateIntuitive(@Query("name") name: string): Promise<number> {
    return this.calculatesService.intuitive(name);
  }

  @Get("/emotional")
  calculateEmotional(@Query("name") name: string): Promise<number> {
    return this.calculatesService.emotional(name);
  }

  @Get("/experience")
  calculateExperience(@Query("name") name: string): Promise<number> {
    return this.calculatesService.experience(name);
  }

  @Get("/birthday")
  calculateBirthday(@Query("day", ParseIntPipe) day: number): Promise<number> {
    return this.calculatesService.birthday(day);
  }

  @Get("/attitude")
  calculateAttitude(
    @Query("day", ParseIntPipe) day: number,
    @Query("month", ParseIntPipe) month: number
  ): Promise<number> {
    return this.calculatesService.attitude(day, month);
  }

  @Get("/peak")
  calculatePeak(@Body("date") date: Date): Promise<any> {
    return this.calculatesService.peak(date);
  }

  @Get("/maturity")
  calculateMaturity(
    @Body("date") date: Date,
    @Body("name") name: string
  ): Promise<any> {
    return this.calculatesService.maturity(name, date);
  }
}
