import { Injectable } from "@nestjs/common";
import { calculateBalanceValue } from "src/utils/calculates/balance-index.calculate";
import { calculateConnectionIndex } from "src/utils/calculates/connection-index.calculate";
import { calculateEmotionalValue } from "src/utils/calculates/emotional-index.calculate";
import { calculateExperienceValue } from "src/utils/calculates/exp-index.calculate";
import { calculateVowelValue } from "src/utils/calculates/internal-index.calculate";
import { calculateIntuitiveValue } from "src/utils/calculates/intuitive-index.calculate";
import { sumDateValues } from "src/utils/calculates/life-path-index.calculate";
import { calculateLogicValue } from "src/utils/calculates/logic-index.calculate";
import { calculateMissIndex } from "src/utils/calculates/miss-index.calculate";
import { calculateNameValue } from "src/utils/calculates/mission-index.calculate";
import { calculateConsonantValue } from "src/utils/calculates/personality-index.calculate";
import { calculateRepeatValue } from "src/utils/calculates/repeat-index.calculate";

import { parseDate } from "src/utils/transformers/parse-date.transformer";
import { DateParts } from "src/utils/types/date-parts.type";
import { calculateDateOfBirthValue } from "../utils/calculates/date-of-birth-index.calculate";
import { calculateAttitudeValue } from "src/utils/calculates/attitude-index.calculate";
import { peakOfLifeValues } from "src/utils/calculates/peak-of-life.calculate";
import { calculateMatureValue } from "src/utils/calculates/connection-dd-sm.calculate";

@Injectable()
export class CalculatesService {
  //chỉ số đường đời
  async lifePath(date: Date): Promise<number> {
    const datePart: DateParts = parseDate(date);
    return sumDateValues(datePart);
  }

  //chỉ số sứ mệnh
  async mission(name: string): Promise<number> {
    return await calculateNameValue(name);
  }

  // chỉ số nội tâm
  async internal(name: string): Promise<number> {
    return await calculateVowelValue(name);
  }

  //chỉ số tương tác
  async personality(name: string): Promise<number> {
    return await calculateConsonantValue(name);
  }

  //Chỉ số kết nối giữa nội tâm và tương tác
  async connection(name: string): Promise<number> {
    return await calculateConnectionIndex(name);
  }

  //chỉ số cân bằng
  async balance(name: string): Promise<number> {
    return await calculateBalanceValue(name);
  }

  // chỉ số thiếu
  async miss(name: string): Promise<number> {
    return await calculateMissIndex(name);
  }

  // chỉ số lặp
  async repeat(name: string): Promise<any> {
    return await calculateRepeatValue(name);
  }

  //chỉ số logic
  async logic(name: string): Promise<number> {
    return await calculateLogicValue(name);
  }

  // chỉ số trực giác
  async intuitive(name: string): Promise<number> {
    return await calculateIntuitiveValue(name);
  }

  //chỉ số cảm xúc
  async emotional(name: string): Promise<number> {
    return await calculateEmotionalValue(name);
  }

  // chỉ số trải nghiệm
  async experience(name: string): Promise<number> {
    return await calculateExperienceValue(name);
  }

  // chỉ số ngày sinh
  async birthday(day: number): Promise<number> {
    return await calculateDateOfBirthValue(day);
  }

  // chỉ số thái độ
  async attitude(day: number, month: number): Promise<number> {
    return await calculateAttitudeValue(day, month);
  }

  // đỉnh đường đời
  async peak(date: Date): Promise<any> {
    const datePart: DateParts = parseDate(date);
    return await peakOfLifeValues(datePart);
  }

  //chỉ số trưởng thành
  async maturity(name: string, date: Date): Promise<any> {
    const datePart: DateParts = parseDate(date);
    return await calculateMatureValue(name, datePart);
  }
}
