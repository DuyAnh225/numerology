import { DateParts } from "../types/date-parts.type";


export function parseDate(date: Date): DateParts {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();

  return { day, month, year };
}
