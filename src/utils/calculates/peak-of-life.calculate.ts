import { DateParts } from "../types/date-parts.type";
import { sumDateValues } from "./life-path-index.calculate";

export function peakOfLifeValues(dateParts: DateParts): any {
  const lifePathIndex = sumDateValues(dateParts);
  const { day, month, year } = dateParts;
  const tmpOne = [...day.toString(), ...month.toString()].map(Number);
  let peakOne = tmpOne.reduce((a, b) => a + b, 0);
  while (peakOne >= 10) {
    let temp = peakOne;
    peakOne = 0;
    while (temp > 0) {
      peakOne += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }
  const peakAgeOne = 36 - lifePathIndex;
  const tmpTwo = [...day.toString(), ...year.toString()].map(Number);
  let peakTwo = tmpTwo.reduce((a, b) => a + b, 0);
  while (peakTwo >= 10) {
    let temp = peakTwo;
    peakTwo = 0;
    while (temp > 0) {
      peakTwo += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }
  const peakAgeTwo = peakAgeOne + 9;
  let peakThree = peakOne + peakTwo;
  while (peakThree >= 10) {
    let temp = peakThree;
    peakThree = 0;
    while (temp > 0) {
      peakThree += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }
  const peakAgeThree = peakAgeTwo + 9;
  const tmpFour = [...year.toString(), ...month.toString()].map(Number);
  let peakFour = tmpFour.reduce((a, b) => a + b, 0);
  while (peakFour >= 10) {
    let temp = peakFour;
    peakFour = 0;
    while (temp > 0) {
      peakFour += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }
  const peakAgeFour = peakAgeThree + 9;
  return {
    peakOne,
    peakAgeOne,
    peakTwo,
    peakAgeTwo,
    peakThree,
    peakAgeThree,
    peakFour,
    peakAgeFour,
  };
}
