import { letterToGroup } from "../groups/letter.group";
import { removeDiacriticsAndSpaces } from "../transformers/removeDiacriticsAndSpaces";

export function calculateRepeatValue(name: string): number | null {
  const NAME = removeDiacriticsAndSpaces(name);

  const uppercaseName = NAME.toUpperCase().replace(/\s/g, "");

  const tmp: number[] = [];

  for (const char of uppercaseName) {
    const groupNumber = Number(letterToGroup[char]);
    if (!isNaN(groupNumber)) {
      tmp.push(groupNumber);
    }
  }

  if (tmp.length === 0) {
    return null; 
  }

  let frequencyMap: { [key: number]: number } = {};

  let maxCount = 0;
  let mostFrequent: number = tmp[0];

  for (let item of tmp) {
    if (!frequencyMap[item]) {
      frequencyMap[item] = 1;
    } else {
      frequencyMap[item]++;
    }

    if (frequencyMap[item] > maxCount) {
      maxCount = frequencyMap[item];
      mostFrequent = item;
    }
  }

  return mostFrequent;
}
