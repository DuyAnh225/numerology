import { letterToGroup } from "../groups/letter.group";

const vowels = ["A", "E", "I", "O", "U"];
const consonants = [
  "B",
  "C",
  "D",
  "F",
  "G",
  "H",
  "J",
  "K",
  "L",
  "M",
  "N",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "V",
  "X",
  "Z",
];

export function calculateBalanceValue(name: string): number {
  const wordsInName = name.split(" ");

  let sum = 0;
  for (let i = 0; i < wordsInName.length; i++) {
    let char = wordsInName[i][0].toUpperCase();

    if (consonants.includes(char) || vowels.includes(char)) {
      const groupNumber = letterToGroup[char];
      if (groupNumber !== undefined) {
        sum += groupNumber;
      }
    }
  }

  while (sum >= 10) {
    let temp = sum;
    sum = 0;
    while (temp > 0) {
      sum += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }

  return sum;
}
