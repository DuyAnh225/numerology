import { vowelToGroup } from "../groups/vowel.group";
import { removeDiacriticsAndSpaces } from "../transformers/removeDiacriticsAndSpaces";

const vowels = ["A", "E", "I", "O", "U"];
const consonants = [
  "B",
  "C",
  "D",
  "F",
  "G",
  "H",
  "J",
  "K",
  "L",
  "M",
  "N",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "V",
  "X",
  "Z",
];

export function calculateVowelValue(name: string): number {
  const NAME = removeDiacriticsAndSpaces(name);

  let sum = 0;
  for (let i = 0; i < NAME.length; i++) {
    let char = NAME[i].toUpperCase();

    // Nếu ký tự là 'Y', kiểm tra ký tự trước đó để xác định nó là nguyên âm hay phụ âm
    if (char === "Y") {
      if (i > 0 && vowels.includes(NAME[i - 1].toUpperCase())) {
        char = "Y_CONSONANT";
      } else {
        char = "Y_VOWEL";
      }
    }

    const groupNumber = vowelToGroup[char];
    if (groupNumber !== undefined) {
      sum += groupNumber;
    }
  }

  while (sum >= 10) {
    let temp = sum;
    sum = 0;
    while (temp > 0) {
      sum += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }

  return sum;
}
