import { emotionalToGroup } from "../groups/emotional.group";
import { removeDiacriticsAndSpaces } from "../transformers/removeDiacriticsAndSpaces";

const consonants = ["S", "B", "T", "O", "X", "Z", "I", "R"];
export function calculateEmotionalValue(name: string): any {
  const wordsInName = name.split(" ");

  let sum = 0;
  for (let i = 0; i < wordsInName.length; i++) {
    let chars = removeDiacriticsAndSpaces(wordsInName[i]);

    for (let j = 0; j < chars.length; j++) {
      if (consonants.includes(chars[j])) {
        const groupNumber = emotionalToGroup[chars[j]];

        if (groupNumber !== undefined) {
          sum += groupNumber;
        }
      }
    }
  }

  while (sum >= 10) {
    let temp = sum;
    sum = 0;
    while (temp > 0) {
      sum += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }

  return sum;
}
