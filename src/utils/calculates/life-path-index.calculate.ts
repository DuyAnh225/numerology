import { DateParts } from "../types/date-parts.type";

export function sumDateValues(dateParts: DateParts): number {
  const { day, month, year } = dateParts;
  const numbers = [
    ...day.toString(),
    ...month.toString(),
    ...year.toString(),
  ].map(Number);

  let sum = numbers.reduce((a, b) => a + b, 0);

  while (sum >= 10) {
    let temp = sum;
    sum = 0;
    while (temp > 0) {
      sum += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }

  return sum;
}
