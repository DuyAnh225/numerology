import { letterToGroup } from "../groups/letter.group";
import { removeDiacriticsAndSpaces } from "../transformers/removeDiacriticsAndSpaces";

const arrNumber: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
export function calculateMissIndex(name: string): any {
  const NAME = removeDiacriticsAndSpaces(name);

  const uppercaseName = NAME.toUpperCase().replace(/\s/g, "");

  const tmp: number[] = [];
  const differences: number[] = [];
  for (const char of uppercaseName) {
    const groupNumber = Number(letterToGroup[char]);
    if (!isNaN(groupNumber)) {
      tmp.push(groupNumber);
    }
  }

  differences.push(...arrNumber.filter((item) => !tmp.includes(item)));

  return differences;
}
