import { calculateVowelValue } from "./internal-index.calculate";
import { calculateConsonantValue } from "./personality-index.calculate";

export function calculateConnectionIndex(name: string): number {
  return Math.abs(calculateVowelValue(name) - calculateConsonantValue(name));
}
