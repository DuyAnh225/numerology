import { consonantToGroup } from "../groups/consonant.group";
import { removeDiacriticsAndSpaces } from "../transformers/removeDiacriticsAndSpaces";

const vowels = ["A", "E", "I", "O", "U"];
const consonants = [
  "B",
  "C",
  "D",
  "F",
  "G",
  "H",
  "J",
  "K",
  "L",
  "M",
  "N",
  "P",
  "Q",
  "R",
  "S",
  "T",
  "V",
  "W",
  "X",
  "Y",
  "Z",
];

export function calculateConsonantValue(name: string): number {
  const NAME = removeDiacriticsAndSpaces(name);

  let sum = 0;
  for (let i = 0; i < NAME.length; i++) {
    let char = NAME[i].toUpperCase();

    // Kiểm tra nếu ký tự hiện tại là phụ âm
    if (consonants.includes(char)) {
      const groupNumber = consonantToGroup[char];
      if (groupNumber !== undefined) {
        sum += groupNumber;
      }
    }
  }

  while (sum >= 10) {
    let temp = sum;
    sum = 0;
    while (temp > 0) {
      sum += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }

  return sum;
}
