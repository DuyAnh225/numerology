import { DateParts } from "../types/date-parts.type";
import { sumDateValues } from "./life-path-index.calculate";
import { calculateNameValue } from "./mission-index.calculate";

export function calculateMatureValue(
  name: string,
  dateParts: DateParts
): number {
  let sum = sumDateValues(dateParts) + calculateNameValue(name);
  while (sum >= 10) {
    let temp = sum;
    sum = 0;
    while (temp > 0) {
      sum += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }
  return sum;
}
