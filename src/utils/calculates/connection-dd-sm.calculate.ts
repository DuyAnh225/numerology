import { DateParts } from "../types/date-parts.type";
import { sumDateValues } from "./life-path-index.calculate";
import { calculateNameValue } from "./mission-index.calculate";

export function calculateMatureValue(
  name: string,
  dateParts: DateParts
): number {
  return Math.abs(sumDateValues(dateParts) + calculateNameValue(name));
}
