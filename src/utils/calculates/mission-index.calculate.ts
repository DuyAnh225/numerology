import { letterToGroup } from "../groups/letter.group";
import { removeDiacriticsAndSpaces } from "../transformers/removeDiacriticsAndSpaces";

export  function calculateNameValue(name: string): number {
  const NAME = removeDiacriticsAndSpaces(name);

  const uppercaseName = NAME.toUpperCase().replace(/\s/g, "");

  let sum = 0;
  for (const char of uppercaseName) {
    const groupNumber = letterToGroup[char];
    if (groupNumber !== undefined) {
      sum += groupNumber;
    }
  }

  while (sum >= 10) {
    let temp = sum;
    sum = 0;
    while (temp > 0) {
      sum += temp % 10;
      temp = Math.floor(temp / 10);
    }
  }

  return sum;
}
