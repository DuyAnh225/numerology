import { consonantGroups } from "../enum/consonants-group.enum";

export const consonantToGroup: { [key: string]: consonantGroups } = {
  J: consonantGroups.Group1,
  S: consonantGroups.Group1,
  B: consonantGroups.Group2,
  K: consonantGroups.Group2,
  T: consonantGroups.Group2,
  C: consonantGroups.Group3,
  L: consonantGroups.Group3,

  D: consonantGroups.Group4,
  M: consonantGroups.Group4,
  V: consonantGroups.Group4,

  N: consonantGroups.Group5,
  W: consonantGroups.Group5,
  F: consonantGroups.Group6,

  X: consonantGroups.Group6,
  G: consonantGroups.Group7,
  P: consonantGroups.Group7,
  Y: consonantGroups.Group7,
  H: consonantGroups.Group8,
  Q: consonantGroups.Group8,
  Z: consonantGroups.Group8,

  R: consonantGroups.Group9,
};
