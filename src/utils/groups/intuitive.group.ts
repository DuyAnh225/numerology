import { intuitiveGroups } from "../enum/intuitive-group.enum";

export const intuitiveToGroup: { [key: string]: intuitiveGroups } = {
  K: intuitiveGroups.Group2,
  F: intuitiveGroups.Group6,
  Q: intuitiveGroups.Group8,
  U: intuitiveGroups.Group3,
  C: intuitiveGroups.Group3,
  V: intuitiveGroups.Group4,
  Y: intuitiveGroups.Group7,
};
