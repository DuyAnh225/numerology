import { LogicGroups } from "../enum/logic-group.enum";

export const logicToGroup: { [key: string]: LogicGroups } = {
  A: LogicGroups.Group1,
  J: LogicGroups.Group1,
  H: LogicGroups.Group8,
  N: LogicGroups.Group5,
  P: LogicGroups.Group7,
  G: LogicGroups.Group7,
  L: LogicGroups.Group3,
};
