import { EmotionalGroups } from "../enum/emotional-group.enum";

export const emotionalToGroup: { [key: string]: EmotionalGroups } = {
  S: EmotionalGroups.Group1,
  B: EmotionalGroups.Group2,
  T: EmotionalGroups.Group2,
  O: EmotionalGroups.Group6,
  X: EmotionalGroups.Group6,
  Z: EmotionalGroups.Group8,
  I: EmotionalGroups.Group9,
  R: EmotionalGroups.Group9,
};
