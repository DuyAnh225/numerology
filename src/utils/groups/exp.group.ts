import { experienceGroups } from "../enum/exp-group.enum";

export const experienceToGroup: { [key: string]: experienceGroups } = {
  D: experienceGroups.Group4,
  M: experienceGroups.Group4,
  E: experienceGroups.Group5,
  W: experienceGroups.Group5,
};
