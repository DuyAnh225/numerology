import { VowelGroups } from "../enum/vowel-group.enum";

export const vowelToGroup: { [key: string]: VowelGroups } = {
  U: VowelGroups.Group1,
  E: VowelGroups.Group2,
  O: VowelGroups.Group3,
  A: VowelGroups.Group4,
  I: VowelGroups.Group5,
  Y: VowelGroups.Group6,
};
