export enum intuitiveGroups {
  // Nhóm 2: K
  Group2 = 2,

  // Nhóm 3:U, C
  Group3 = 3,

  // Nhóm 4:V
  Group4 = 4,

  // Nhóm 6: F
  Group6 = 6,

  // Nhóm 7: Y
  Group7 = 7,

  // Nhóm 8:Q
  Group8 = 8,
}
