export enum consonantGroups {
  // Nhóm 1: J, S
  Group1 = 1,

  // Nhóm 2: B, K, T
  Group2 = 2,

  // Nhóm 3: C, D,
  Group3 = 3,

  // Nhóm 4: F, G, H
  Group4 = 4,

  // Nhóm 5: L, M
  Group5 = 5,

  // Nhóm 6: N,  P
  Group6 = 6,

  // Nhóm 7: Q, R
  Group7 = 7,

  // Nhóm 8: V, W, X
  Group8 = 8,

  // Nhóm 9: Y, Z
  Group9 = 9,
}
