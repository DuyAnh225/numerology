export enum EmotionalGroups {
  //S
  Group1 = 1,

  //B, T
  Group2 = 2,

  // Nhóm 6: O, X
  Group6 = 6,

  // Nhóm 8: Z
  Group8 = 8,

  // Nhóm 9: I, R
  Group9 = 9,
}
