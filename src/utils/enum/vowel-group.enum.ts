export enum VowelGroups {
  //U
  Group1 = 3,

  //E
  Group2 = 5,

  //O
  Group3 = 6,

  //A
  Group4 = 1,

  //I
  Group5 = 9,

  //Y
  Group6 = 7,
}
