export enum LetterGroups {
  // Nhóm 1: A, J, S
  Group1 = 1,

  // Nhóm 2: B, K, T
  Group2 = 2,

  // Nhóm 3: C, D, E
  Group3 = 3,

  // Nhóm 4: F, G, H
  Group4 = 4,

  // Nhóm 5: I, L, M
  Group5 = 5,

  // Nhóm 6: N, O, P
  Group6 = 6,

  // Nhóm 7: Q, R, U
  Group7 = 7,

  // Nhóm 8: V, W, X
  Group8 = 8,

  // Nhóm 9: Y, Z
  Group9 = 9,
}
