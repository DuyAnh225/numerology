export enum LogicGroups {
  //A, J
  Group1 = 1,
  //H
  Group8 = 8,
  //N
  Group5 = 5,
  //P, G
  Group7 = 7,
  //L
  Group3 = 3,
}
