"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmotionalGroups = void 0;
var EmotionalGroups;
(function (EmotionalGroups) {
    EmotionalGroups[EmotionalGroups["Group1"] = 1] = "Group1";
    EmotionalGroups[EmotionalGroups["Group2"] = 2] = "Group2";
    EmotionalGroups[EmotionalGroups["Group6"] = 6] = "Group6";
    EmotionalGroups[EmotionalGroups["Group8"] = 8] = "Group8";
    EmotionalGroups[EmotionalGroups["Group9"] = 9] = "Group9";
})(EmotionalGroups || (exports.EmotionalGroups = EmotionalGroups = {}));
//# sourceMappingURL=emotional-group.enum.js.map