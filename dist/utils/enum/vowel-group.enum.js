"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VowelGroups = void 0;
var VowelGroups;
(function (VowelGroups) {
    VowelGroups[VowelGroups["Group1"] = 3] = "Group1";
    VowelGroups[VowelGroups["Group2"] = 5] = "Group2";
    VowelGroups[VowelGroups["Group3"] = 6] = "Group3";
    VowelGroups[VowelGroups["Group4"] = 1] = "Group4";
    VowelGroups[VowelGroups["Group5"] = 9] = "Group5";
    VowelGroups[VowelGroups["Group6"] = 7] = "Group6";
})(VowelGroups || (exports.VowelGroups = VowelGroups = {}));
//# sourceMappingURL=vowel-group.enum.js.map