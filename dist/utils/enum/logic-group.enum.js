"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LogicGroups = void 0;
var LogicGroups;
(function (LogicGroups) {
    LogicGroups[LogicGroups["Group1"] = 1] = "Group1";
    LogicGroups[LogicGroups["Group8"] = 8] = "Group8";
    LogicGroups[LogicGroups["Group5"] = 5] = "Group5";
    LogicGroups[LogicGroups["Group7"] = 7] = "Group7";
    LogicGroups[LogicGroups["Group3"] = 3] = "Group3";
})(LogicGroups || (exports.LogicGroups = LogicGroups = {}));
//# sourceMappingURL=logic-group.enum.js.map