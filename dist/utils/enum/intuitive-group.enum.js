"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.intuitiveGroups = void 0;
var intuitiveGroups;
(function (intuitiveGroups) {
    intuitiveGroups[intuitiveGroups["Group2"] = 2] = "Group2";
    intuitiveGroups[intuitiveGroups["Group3"] = 3] = "Group3";
    intuitiveGroups[intuitiveGroups["Group4"] = 4] = "Group4";
    intuitiveGroups[intuitiveGroups["Group6"] = 6] = "Group6";
    intuitiveGroups[intuitiveGroups["Group7"] = 7] = "Group7";
    intuitiveGroups[intuitiveGroups["Group8"] = 8] = "Group8";
})(intuitiveGroups || (exports.intuitiveGroups = intuitiveGroups = {}));
//# sourceMappingURL=intuitive-group.enum.js.map