"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LetterGroups = void 0;
var LetterGroups;
(function (LetterGroups) {
    LetterGroups[LetterGroups["Group1"] = 1] = "Group1";
    LetterGroups[LetterGroups["Group2"] = 2] = "Group2";
    LetterGroups[LetterGroups["Group3"] = 3] = "Group3";
    LetterGroups[LetterGroups["Group4"] = 4] = "Group4";
    LetterGroups[LetterGroups["Group5"] = 5] = "Group5";
    LetterGroups[LetterGroups["Group6"] = 6] = "Group6";
    LetterGroups[LetterGroups["Group7"] = 7] = "Group7";
    LetterGroups[LetterGroups["Group8"] = 8] = "Group8";
    LetterGroups[LetterGroups["Group9"] = 9] = "Group9";
})(LetterGroups || (exports.LetterGroups = LetterGroups = {}));
//# sourceMappingURL=letter-group.enum.js.map