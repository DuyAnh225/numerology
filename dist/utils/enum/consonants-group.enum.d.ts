export declare enum consonantGroups {
    Group1 = 1,
    Group2 = 2,
    Group3 = 3,
    Group4 = 4,
    Group5 = 5,
    Group6 = 6,
    Group7 = 7,
    Group8 = 8,
    Group9 = 9
}
