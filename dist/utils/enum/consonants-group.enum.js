"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.consonantGroups = void 0;
var consonantGroups;
(function (consonantGroups) {
    consonantGroups[consonantGroups["Group1"] = 1] = "Group1";
    consonantGroups[consonantGroups["Group2"] = 2] = "Group2";
    consonantGroups[consonantGroups["Group3"] = 3] = "Group3";
    consonantGroups[consonantGroups["Group4"] = 4] = "Group4";
    consonantGroups[consonantGroups["Group5"] = 5] = "Group5";
    consonantGroups[consonantGroups["Group6"] = 6] = "Group6";
    consonantGroups[consonantGroups["Group7"] = 7] = "Group7";
    consonantGroups[consonantGroups["Group8"] = 8] = "Group8";
    consonantGroups[consonantGroups["Group9"] = 9] = "Group9";
})(consonantGroups || (exports.consonantGroups = consonantGroups = {}));
//# sourceMappingURL=consonants-group.enum.js.map