import { LetterGroups } from "../enum/letter-group.enum";
export declare const letterToGroup: {
    [key: string]: LetterGroups;
};
