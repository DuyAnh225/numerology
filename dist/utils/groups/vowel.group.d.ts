import { VowelGroups } from "../enum/vowel-group.enum";
export declare const vowelToGroup: {
    [key: string]: VowelGroups;
};
