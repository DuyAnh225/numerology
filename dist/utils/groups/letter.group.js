"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.letterToGroup = void 0;
const letter_group_enum_1 = require("../enum/letter-group.enum");
exports.letterToGroup = {
    A: letter_group_enum_1.LetterGroups.Group1,
    J: letter_group_enum_1.LetterGroups.Group1,
    S: letter_group_enum_1.LetterGroups.Group1,
    B: letter_group_enum_1.LetterGroups.Group2,
    K: letter_group_enum_1.LetterGroups.Group2,
    T: letter_group_enum_1.LetterGroups.Group2,
    C: letter_group_enum_1.LetterGroups.Group3,
    L: letter_group_enum_1.LetterGroups.Group3,
    U: letter_group_enum_1.LetterGroups.Group3,
    D: letter_group_enum_1.LetterGroups.Group4,
    M: letter_group_enum_1.LetterGroups.Group4,
    V: letter_group_enum_1.LetterGroups.Group4,
    E: letter_group_enum_1.LetterGroups.Group5,
    N: letter_group_enum_1.LetterGroups.Group5,
    W: letter_group_enum_1.LetterGroups.Group5,
    F: letter_group_enum_1.LetterGroups.Group6,
    O: letter_group_enum_1.LetterGroups.Group6,
    X: letter_group_enum_1.LetterGroups.Group6,
    G: letter_group_enum_1.LetterGroups.Group7,
    P: letter_group_enum_1.LetterGroups.Group7,
    Y: letter_group_enum_1.LetterGroups.Group7,
    H: letter_group_enum_1.LetterGroups.Group8,
    Q: letter_group_enum_1.LetterGroups.Group8,
    Z: letter_group_enum_1.LetterGroups.Group8,
    I: letter_group_enum_1.LetterGroups.Group9,
    R: letter_group_enum_1.LetterGroups.Group9,
};
//# sourceMappingURL=letter.group.js.map