import { LogicGroups } from "../enum/logic-group.enum";
export declare const logicToGroup: {
    [key: string]: LogicGroups;
};
