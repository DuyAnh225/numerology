import { experienceGroups } from "../enum/exp-group.enum";
export declare const experienceToGroup: {
    [key: string]: experienceGroups;
};
