"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.intuitiveToGroup = void 0;
const intuitive_group_enum_1 = require("../enum/intuitive-group.enum");
exports.intuitiveToGroup = {
    K: intuitive_group_enum_1.intuitiveGroups.Group2,
    F: intuitive_group_enum_1.intuitiveGroups.Group6,
    Q: intuitive_group_enum_1.intuitiveGroups.Group8,
    U: intuitive_group_enum_1.intuitiveGroups.Group3,
    C: intuitive_group_enum_1.intuitiveGroups.Group3,
    V: intuitive_group_enum_1.intuitiveGroups.Group4,
    Y: intuitive_group_enum_1.intuitiveGroups.Group7,
};
//# sourceMappingURL=intuitive.group.js.map