"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.consonantToGroup = void 0;
const consonants_group_enum_1 = require("../enum/consonants-group.enum");
exports.consonantToGroup = {
    J: consonants_group_enum_1.consonantGroups.Group1,
    S: consonants_group_enum_1.consonantGroups.Group1,
    B: consonants_group_enum_1.consonantGroups.Group2,
    K: consonants_group_enum_1.consonantGroups.Group2,
    T: consonants_group_enum_1.consonantGroups.Group2,
    C: consonants_group_enum_1.consonantGroups.Group3,
    L: consonants_group_enum_1.consonantGroups.Group3,
    D: consonants_group_enum_1.consonantGroups.Group4,
    M: consonants_group_enum_1.consonantGroups.Group4,
    V: consonants_group_enum_1.consonantGroups.Group4,
    N: consonants_group_enum_1.consonantGroups.Group5,
    W: consonants_group_enum_1.consonantGroups.Group5,
    F: consonants_group_enum_1.consonantGroups.Group6,
    X: consonants_group_enum_1.consonantGroups.Group6,
    G: consonants_group_enum_1.consonantGroups.Group7,
    P: consonants_group_enum_1.consonantGroups.Group7,
    Y: consonants_group_enum_1.consonantGroups.Group7,
    H: consonants_group_enum_1.consonantGroups.Group8,
    Q: consonants_group_enum_1.consonantGroups.Group8,
    Z: consonants_group_enum_1.consonantGroups.Group8,
    R: consonants_group_enum_1.consonantGroups.Group9,
};
//# sourceMappingURL=consonant.group.js.map