"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.vowelToGroup = void 0;
const vowel_group_enum_1 = require("../enum/vowel-group.enum");
exports.vowelToGroup = {
    U: vowel_group_enum_1.VowelGroups.Group1,
    E: vowel_group_enum_1.VowelGroups.Group2,
    O: vowel_group_enum_1.VowelGroups.Group3,
    A: vowel_group_enum_1.VowelGroups.Group4,
    I: vowel_group_enum_1.VowelGroups.Group5,
    Y: vowel_group_enum_1.VowelGroups.Group6,
};
//# sourceMappingURL=vowel.group.js.map