import { intuitiveGroups } from "../enum/intuitive-group.enum";
export declare const intuitiveToGroup: {
    [key: string]: intuitiveGroups;
};
