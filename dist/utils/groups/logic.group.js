"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.logicToGroup = void 0;
const logic_group_enum_1 = require("../enum/logic-group.enum");
exports.logicToGroup = {
    A: logic_group_enum_1.LogicGroups.Group1,
    J: logic_group_enum_1.LogicGroups.Group1,
    H: logic_group_enum_1.LogicGroups.Group8,
    N: logic_group_enum_1.LogicGroups.Group5,
    P: logic_group_enum_1.LogicGroups.Group7,
    G: logic_group_enum_1.LogicGroups.Group7,
    L: logic_group_enum_1.LogicGroups.Group3,
};
//# sourceMappingURL=logic.group.js.map