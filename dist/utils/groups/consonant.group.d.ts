import { consonantGroups } from "../enum/consonants-group.enum";
export declare const consonantToGroup: {
    [key: string]: consonantGroups;
};
