"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.experienceToGroup = void 0;
const exp_group_enum_1 = require("../enum/exp-group.enum");
exports.experienceToGroup = {
    D: exp_group_enum_1.experienceGroups.Group4,
    M: exp_group_enum_1.experienceGroups.Group4,
    E: exp_group_enum_1.experienceGroups.Group5,
    W: exp_group_enum_1.experienceGroups.Group5,
};
//# sourceMappingURL=exp.group.js.map