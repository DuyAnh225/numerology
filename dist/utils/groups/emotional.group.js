"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emotionalToGroup = void 0;
const emotional_group_enum_1 = require("../enum/emotional-group.enum");
exports.emotionalToGroup = {
    S: emotional_group_enum_1.EmotionalGroups.Group1,
    B: emotional_group_enum_1.EmotionalGroups.Group2,
    T: emotional_group_enum_1.EmotionalGroups.Group2,
    O: emotional_group_enum_1.EmotionalGroups.Group6,
    X: emotional_group_enum_1.EmotionalGroups.Group6,
    Z: emotional_group_enum_1.EmotionalGroups.Group8,
    I: emotional_group_enum_1.EmotionalGroups.Group9,
    R: emotional_group_enum_1.EmotionalGroups.Group9,
};
//# sourceMappingURL=emotional.group.js.map