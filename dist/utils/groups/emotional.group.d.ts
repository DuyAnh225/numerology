import { EmotionalGroups } from "../enum/emotional-group.enum";
export declare const emotionalToGroup: {
    [key: string]: EmotionalGroups;
};
