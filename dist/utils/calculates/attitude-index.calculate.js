"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateAttitudeValue = void 0;
function calculateAttitudeValue(day, month) {
    let sumDay = 0;
    let sumMonth = 0;
    let sum = 0;
    while (day > 0) {
        sumDay += day % 10;
        day = Math.floor(day / 10);
    }
    while (month > 0) {
        sumMonth += month % 10;
        month = Math.floor(month / 10);
    }
    sum = sumDay + sumMonth;
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateAttitudeValue = calculateAttitudeValue;
//# sourceMappingURL=attitude-index.calculate.js.map