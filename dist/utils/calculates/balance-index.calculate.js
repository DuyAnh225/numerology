"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateBalanceValue = void 0;
const letter_group_1 = require("../groups/letter.group");
const vowels = ["A", "E", "I", "O", "U"];
const consonants = [
    "B",
    "C",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "M",
    "N",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "V",
    "X",
    "Z",
];
function calculateBalanceValue(name) {
    const wordsInName = name.split(" ");
    let sum = 0;
    for (let i = 0; i < wordsInName.length; i++) {
        let char = wordsInName[i][0].toUpperCase();
        if (consonants.includes(char) || vowels.includes(char)) {
            const groupNumber = letter_group_1.letterToGroup[char];
            if (groupNumber !== undefined) {
                sum += groupNumber;
            }
        }
    }
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateBalanceValue = calculateBalanceValue;
//# sourceMappingURL=balance-index.calculate.js.map