"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateMissIndex = void 0;
const letter_group_1 = require("../groups/letter.group");
const removeDiacriticsAndSpaces_1 = require("../transformers/removeDiacriticsAndSpaces");
const arrNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9];
function calculateMissIndex(name) {
    const NAME = (0, removeDiacriticsAndSpaces_1.removeDiacriticsAndSpaces)(name);
    const uppercaseName = NAME.toUpperCase().replace(/\s/g, "");
    const tmp = [];
    const differences = [];
    for (const char of uppercaseName) {
        const groupNumber = Number(letter_group_1.letterToGroup[char]);
        if (!isNaN(groupNumber)) {
            tmp.push(groupNumber);
        }
    }
    differences.push(...arrNumber.filter((item) => !tmp.includes(item)));
    return differences;
}
exports.calculateMissIndex = calculateMissIndex;
//# sourceMappingURL=miss-index.calculate.js.map