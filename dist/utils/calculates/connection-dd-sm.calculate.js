"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateMatureValue = void 0;
const life_path_index_calculate_1 = require("./life-path-index.calculate");
const mission_index_calculate_1 = require("./mission-index.calculate");
function calculateMatureValue(name, dateParts) {
    return Math.abs((0, life_path_index_calculate_1.sumDateValues)(dateParts) + (0, mission_index_calculate_1.calculateNameValue)(name));
}
exports.calculateMatureValue = calculateMatureValue;
//# sourceMappingURL=connection-dd-sm.calculate.js.map