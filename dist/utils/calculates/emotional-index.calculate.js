"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateEmotionalValue = void 0;
const emotional_group_1 = require("../groups/emotional.group");
const removeDiacriticsAndSpaces_1 = require("../transformers/removeDiacriticsAndSpaces");
const consonants = ["S", "B", "T", "O", "X", "Z", "I", "R"];
function calculateEmotionalValue(name) {
    const wordsInName = name.split(" ");
    let sum = 0;
    for (let i = 0; i < wordsInName.length; i++) {
        let chars = (0, removeDiacriticsAndSpaces_1.removeDiacriticsAndSpaces)(wordsInName[i]);
        for (let j = 0; j < chars.length; j++) {
            if (consonants.includes(chars[j])) {
                const groupNumber = emotional_group_1.emotionalToGroup[chars[j]];
                if (groupNumber !== undefined) {
                    sum += groupNumber;
                }
            }
        }
    }
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateEmotionalValue = calculateEmotionalValue;
//# sourceMappingURL=emotional-index.calculate.js.map