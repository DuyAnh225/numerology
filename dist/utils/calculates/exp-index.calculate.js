"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateExperienceValue = void 0;
const exp_group_1 = require("../groups/exp.group");
const removeDiacriticsAndSpaces_1 = require("../transformers/removeDiacriticsAndSpaces");
const consonants = ["D", "M", "E", "W"];
function calculateExperienceValue(name) {
    const wordsInName = name.split(" ");
    let sum = 0;
    for (let i = 0; i < wordsInName.length; i++) {
        let chars = (0, removeDiacriticsAndSpaces_1.removeDiacriticsAndSpaces)(wordsInName[i]);
        for (let j = 0; j < chars.length; j++) {
            if (consonants.includes(chars[j])) {
                const groupNumber = exp_group_1.experienceToGroup[chars[j]];
                if (groupNumber !== undefined) {
                    sum += groupNumber;
                }
            }
        }
    }
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    if (sum == 0) {
        return 1;
    }
    return sum;
}
exports.calculateExperienceValue = calculateExperienceValue;
//# sourceMappingURL=exp-index.calculate.js.map