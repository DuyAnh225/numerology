"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateNameValue = void 0;
const letter_group_1 = require("../groups/letter.group");
const removeDiacriticsAndSpaces_1 = require("../transformers/removeDiacriticsAndSpaces");
function calculateNameValue(name) {
    const NAME = (0, removeDiacriticsAndSpaces_1.removeDiacriticsAndSpaces)(name);
    const uppercaseName = NAME.toUpperCase().replace(/\s/g, "");
    let sum = 0;
    for (const char of uppercaseName) {
        const groupNumber = letter_group_1.letterToGroup[char];
        if (groupNumber !== undefined) {
            sum += groupNumber;
        }
    }
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateNameValue = calculateNameValue;
//# sourceMappingURL=mission-index.calculate.js.map