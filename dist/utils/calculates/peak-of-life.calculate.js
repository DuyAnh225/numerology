"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.peakOfLifeValues = void 0;
const life_path_index_calculate_1 = require("./life-path-index.calculate");
function peakOfLifeValues(dateParts) {
    const lifePathIndex = (0, life_path_index_calculate_1.sumDateValues)(dateParts);
    const { day, month, year } = dateParts;
    const tmpOne = [...day.toString(), ...month.toString()].map(Number);
    let peakOne = tmpOne.reduce((a, b) => a + b, 0);
    while (peakOne >= 10) {
        let temp = peakOne;
        peakOne = 0;
        while (temp > 0) {
            peakOne += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    const peakAgeOne = 36 - lifePathIndex;
    const tmpTwo = [...day.toString(), ...year.toString()].map(Number);
    let peakTwo = tmpTwo.reduce((a, b) => a + b, 0);
    while (peakTwo >= 10) {
        let temp = peakTwo;
        peakTwo = 0;
        while (temp > 0) {
            peakTwo += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    const peakAgeTwo = peakAgeOne + 9;
    let peakThree = peakOne + peakTwo;
    while (peakThree >= 10) {
        let temp = peakThree;
        peakThree = 0;
        while (temp > 0) {
            peakThree += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    const peakAgeThree = peakAgeTwo + 9;
    const tmpFour = [...year.toString(), ...month.toString()].map(Number);
    let peakFour = tmpFour.reduce((a, b) => a + b, 0);
    while (peakFour >= 10) {
        let temp = peakFour;
        peakFour = 0;
        while (temp > 0) {
            peakFour += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    const peakAgeFour = peakAgeThree + 9;
    return {
        peakOne,
        peakAgeOne,
        peakTwo,
        peakAgeTwo,
        peakThree,
        peakAgeThree,
        peakFour,
        peakAgeFour,
    };
}
exports.peakOfLifeValues = peakOfLifeValues;
//# sourceMappingURL=peak-of-life.calculate.js.map