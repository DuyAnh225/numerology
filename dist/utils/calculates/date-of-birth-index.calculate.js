"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateDateOfBirthValue = void 0;
function calculateDateOfBirthValue(day) {
    let sum = day;
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateDateOfBirthValue = calculateDateOfBirthValue;
//# sourceMappingURL=date-of-birth-index.calculate.js.map