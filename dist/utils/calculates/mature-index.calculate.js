"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateMatureValue = void 0;
const life_path_index_calculate_1 = require("./life-path-index.calculate");
const mission_index_calculate_1 = require("./mission-index.calculate");
function calculateMatureValue(name, dateParts) {
    let sum = (0, life_path_index_calculate_1.sumDateValues)(dateParts) + (0, mission_index_calculate_1.calculateNameValue)(name);
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateMatureValue = calculateMatureValue;
//# sourceMappingURL=mature-index.calculate.js.map