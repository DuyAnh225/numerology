import { DateParts } from "../types/date-parts.type";
export declare function calculateMatureValue(name: string, dateParts: DateParts): number;
