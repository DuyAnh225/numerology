"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateConsonantValue = void 0;
const consonant_group_1 = require("../groups/consonant.group");
const removeDiacriticsAndSpaces_1 = require("../transformers/removeDiacriticsAndSpaces");
const vowels = ["A", "E", "I", "O", "U"];
const consonants = [
    "B",
    "C",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "M",
    "N",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "V",
    "W",
    "X",
    "Y",
    "Z",
];
function calculateConsonantValue(name) {
    const NAME = (0, removeDiacriticsAndSpaces_1.removeDiacriticsAndSpaces)(name);
    let sum = 0;
    for (let i = 0; i < NAME.length; i++) {
        let char = NAME[i].toUpperCase();
        if (consonants.includes(char)) {
            const groupNumber = consonant_group_1.consonantToGroup[char];
            if (groupNumber !== undefined) {
                sum += groupNumber;
            }
        }
    }
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateConsonantValue = calculateConsonantValue;
//# sourceMappingURL=personality-index.calculate.js.map