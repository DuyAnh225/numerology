"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sumDateValues = void 0;
function sumDateValues(dateParts) {
    const { day, month, year } = dateParts;
    const numbers = [
        ...day.toString(),
        ...month.toString(),
        ...year.toString(),
    ].map(Number);
    let sum = numbers.reduce((a, b) => a + b, 0);
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.sumDateValues = sumDateValues;
//# sourceMappingURL=life-path-index.calculate.js.map