"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateConnectionIndex = void 0;
const internal_index_calculate_1 = require("./internal-index.calculate");
const personality_index_calculate_1 = require("./personality-index.calculate");
function calculateConnectionIndex(name) {
    return Math.abs((0, internal_index_calculate_1.calculateVowelValue)(name) - (0, personality_index_calculate_1.calculateConsonantValue)(name));
}
exports.calculateConnectionIndex = calculateConnectionIndex;
//# sourceMappingURL=connection-index.calculate.js.map