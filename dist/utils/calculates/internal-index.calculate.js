"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateVowelValue = void 0;
const vowel_group_1 = require("../groups/vowel.group");
const removeDiacriticsAndSpaces_1 = require("../transformers/removeDiacriticsAndSpaces");
const vowels = ["A", "E", "I", "O", "U"];
const consonants = [
    "B",
    "C",
    "D",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "M",
    "N",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "V",
    "X",
    "Z",
];
function calculateVowelValue(name) {
    const NAME = (0, removeDiacriticsAndSpaces_1.removeDiacriticsAndSpaces)(name);
    let sum = 0;
    for (let i = 0; i < NAME.length; i++) {
        let char = NAME[i].toUpperCase();
        if (char === "Y") {
            if (i > 0 && vowels.includes(NAME[i - 1].toUpperCase())) {
                char = "Y_CONSONANT";
            }
            else {
                char = "Y_VOWEL";
            }
        }
        const groupNumber = vowel_group_1.vowelToGroup[char];
        if (groupNumber !== undefined) {
            sum += groupNumber;
        }
    }
    while (sum >= 10) {
        let temp = sum;
        sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp = Math.floor(temp / 10);
        }
    }
    return sum;
}
exports.calculateVowelValue = calculateVowelValue;
//# sourceMappingURL=internal-index.calculate.js.map