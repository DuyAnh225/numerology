"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.calculateRepeatValue = void 0;
const letter_group_1 = require("../groups/letter.group");
const removeDiacriticsAndSpaces_1 = require("../transformers/removeDiacriticsAndSpaces");
function calculateRepeatValue(name) {
    const NAME = (0, removeDiacriticsAndSpaces_1.removeDiacriticsAndSpaces)(name);
    const uppercaseName = NAME.toUpperCase().replace(/\s/g, "");
    const tmp = [];
    for (const char of uppercaseName) {
        const groupNumber = Number(letter_group_1.letterToGroup[char]);
        if (!isNaN(groupNumber)) {
            tmp.push(groupNumber);
        }
    }
    if (tmp.length === 0) {
        return null;
    }
    let frequencyMap = {};
    let maxCount = 0;
    let mostFrequent = tmp[0];
    for (let item of tmp) {
        if (!frequencyMap[item]) {
            frequencyMap[item] = 1;
        }
        else {
            frequencyMap[item]++;
        }
        if (frequencyMap[item] > maxCount) {
            maxCount = frequencyMap[item];
            mostFrequent = item;
        }
    }
    return mostFrequent;
}
exports.calculateRepeatValue = calculateRepeatValue;
//# sourceMappingURL=repeat-index.calculate.js.map