export type DateParts = {
    day: number;
    month: number;
    year: number;
};
