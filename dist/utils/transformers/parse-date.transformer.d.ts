import { DateParts } from "../types/date-parts.type";
export declare function parseDate(date: Date): DateParts;
