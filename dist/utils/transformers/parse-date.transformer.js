"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseDate = void 0;
function parseDate(date) {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    return { day, month, year };
}
exports.parseDate = parseDate;
//# sourceMappingURL=parse-date.transformer.js.map