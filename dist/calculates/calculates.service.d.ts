export declare class CalculatesService {
    lifePath(date: Date): Promise<number>;
    mission(name: string): Promise<number>;
    internal(name: string): Promise<number>;
    personality(name: string): Promise<number>;
    connection(name: string): Promise<number>;
    balance(name: string): Promise<number>;
    miss(name: string): Promise<number>;
    repeat(name: string): Promise<any>;
    logic(name: string): Promise<number>;
    intuitive(name: string): Promise<number>;
    emotional(name: string): Promise<number>;
    experience(name: string): Promise<number>;
    birthday(day: number): Promise<number>;
    attitude(day: number, month: number): Promise<number>;
    peak(date: Date): Promise<any>;
    maturity(name: string, date: Date): Promise<any>;
}
