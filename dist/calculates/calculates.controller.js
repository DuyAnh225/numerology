"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CalculatesController = void 0;
const common_1 = require("@nestjs/common");
const calculates_service_1 = require("./calculates.service");
let CalculatesController = exports.CalculatesController = class CalculatesController {
    constructor(calculatesService) {
        this.calculatesService = calculatesService;
    }
    calculateLifePath(date) {
        return this.calculatesService.lifePath(date);
    }
    calculateMission(name) {
        return this.calculatesService.mission(name);
    }
    calculateInternal(name) {
        return this.calculatesService.internal(name);
    }
    calculatePersonality(name) {
        return this.calculatesService.personality(name);
    }
    calculateConnection(name) {
        return this.calculatesService.connection(name);
    }
    calculateBalance(name) {
        return this.calculatesService.balance(name);
    }
    calculateMiss(name) {
        return this.calculatesService.miss(name);
    }
    calculateRepeat(name) {
        return this.calculatesService.repeat(name);
    }
    calculateLogic(name) {
        return this.calculatesService.logic(name);
    }
    calculateIntuitive(name) {
        return this.calculatesService.intuitive(name);
    }
    calculateEmotional(name) {
        return this.calculatesService.emotional(name);
    }
    calculateExperience(name) {
        return this.calculatesService.experience(name);
    }
    calculateBirthday(day) {
        return this.calculatesService.birthday(day);
    }
    calculateAttitude(day, month) {
        return this.calculatesService.attitude(day, month);
    }
    calculatePeak(date) {
        return this.calculatesService.peak(date);
    }
    calculateMaturity(date, name) {
        return this.calculatesService.maturity(name, date);
    }
};
__decorate([
    (0, common_1.Get)("/life-path"),
    __param(0, (0, common_1.Body)("date")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Date]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateLifePath", null);
__decorate([
    (0, common_1.Get)("/mission"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateMission", null);
__decorate([
    (0, common_1.Get)("/internal"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateInternal", null);
__decorate([
    (0, common_1.Get)("/personality"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculatePersonality", null);
__decorate([
    (0, common_1.Get)("/connection"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateConnection", null);
__decorate([
    (0, common_1.Get)("/balance"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateBalance", null);
__decorate([
    (0, common_1.Get)("/miss"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateMiss", null);
__decorate([
    (0, common_1.Get)("/repeat"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateRepeat", null);
__decorate([
    (0, common_1.Get)("/logic"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateLogic", null);
__decorate([
    (0, common_1.Get)("/intuitive"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateIntuitive", null);
__decorate([
    (0, common_1.Get)("/emotional"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateEmotional", null);
__decorate([
    (0, common_1.Get)("/experience"),
    __param(0, (0, common_1.Query)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateExperience", null);
__decorate([
    (0, common_1.Get)("/birthday"),
    __param(0, (0, common_1.Query)("day", common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateBirthday", null);
__decorate([
    (0, common_1.Get)("/attitude"),
    __param(0, (0, common_1.Query)("day", common_1.ParseIntPipe)),
    __param(1, (0, common_1.Query)("month", common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, Number]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateAttitude", null);
__decorate([
    (0, common_1.Get)("/peak"),
    __param(0, (0, common_1.Body)("date")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Date]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculatePeak", null);
__decorate([
    (0, common_1.Get)("/maturity"),
    __param(0, (0, common_1.Body)("date")),
    __param(1, (0, common_1.Body)("name")),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Date, String]),
    __metadata("design:returntype", Promise)
], CalculatesController.prototype, "calculateMaturity", null);
exports.CalculatesController = CalculatesController = __decorate([
    (0, common_1.Controller)("calculates"),
    __metadata("design:paramtypes", [calculates_service_1.CalculatesService])
], CalculatesController);
//# sourceMappingURL=calculates.controller.js.map