"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CalculatesService = void 0;
const common_1 = require("@nestjs/common");
const balance_index_calculate_1 = require("../utils/calculates/balance-index.calculate");
const connection_index_calculate_1 = require("../utils/calculates/connection-index.calculate");
const emotional_index_calculate_1 = require("../utils/calculates/emotional-index.calculate");
const exp_index_calculate_1 = require("../utils/calculates/exp-index.calculate");
const internal_index_calculate_1 = require("../utils/calculates/internal-index.calculate");
const intuitive_index_calculate_1 = require("../utils/calculates/intuitive-index.calculate");
const life_path_index_calculate_1 = require("../utils/calculates/life-path-index.calculate");
const logic_index_calculate_1 = require("../utils/calculates/logic-index.calculate");
const miss_index_calculate_1 = require("../utils/calculates/miss-index.calculate");
const mission_index_calculate_1 = require("../utils/calculates/mission-index.calculate");
const personality_index_calculate_1 = require("../utils/calculates/personality-index.calculate");
const repeat_index_calculate_1 = require("../utils/calculates/repeat-index.calculate");
const parse_date_transformer_1 = require("../utils/transformers/parse-date.transformer");
const date_of_birth_index_calculate_1 = require("../utils/calculates/date-of-birth-index.calculate");
const attitude_index_calculate_1 = require("../utils/calculates/attitude-index.calculate");
const peak_of_life_calculate_1 = require("../utils/calculates/peak-of-life.calculate");
const connection_dd_sm_calculate_1 = require("../utils/calculates/connection-dd-sm.calculate");
let CalculatesService = exports.CalculatesService = class CalculatesService {
    async lifePath(date) {
        const datePart = (0, parse_date_transformer_1.parseDate)(date);
        return (0, life_path_index_calculate_1.sumDateValues)(datePart);
    }
    async mission(name) {
        return await (0, mission_index_calculate_1.calculateNameValue)(name);
    }
    async internal(name) {
        return await (0, internal_index_calculate_1.calculateVowelValue)(name);
    }
    async personality(name) {
        return await (0, personality_index_calculate_1.calculateConsonantValue)(name);
    }
    async connection(name) {
        return await (0, connection_index_calculate_1.calculateConnectionIndex)(name);
    }
    async balance(name) {
        return await (0, balance_index_calculate_1.calculateBalanceValue)(name);
    }
    async miss(name) {
        return await (0, miss_index_calculate_1.calculateMissIndex)(name);
    }
    async repeat(name) {
        return await (0, repeat_index_calculate_1.calculateRepeatValue)(name);
    }
    async logic(name) {
        return await (0, logic_index_calculate_1.calculateLogicValue)(name);
    }
    async intuitive(name) {
        return await (0, intuitive_index_calculate_1.calculateIntuitiveValue)(name);
    }
    async emotional(name) {
        return await (0, emotional_index_calculate_1.calculateEmotionalValue)(name);
    }
    async experience(name) {
        return await (0, exp_index_calculate_1.calculateExperienceValue)(name);
    }
    async birthday(day) {
        return await (0, date_of_birth_index_calculate_1.calculateDateOfBirthValue)(day);
    }
    async attitude(day, month) {
        return await (0, attitude_index_calculate_1.calculateAttitudeValue)(day, month);
    }
    async peak(date) {
        const datePart = (0, parse_date_transformer_1.parseDate)(date);
        return await (0, peak_of_life_calculate_1.peakOfLifeValues)(datePart);
    }
    async maturity(name, date) {
        const datePart = (0, parse_date_transformer_1.parseDate)(date);
        return await (0, connection_dd_sm_calculate_1.calculateMatureValue)(name, datePart);
    }
};
exports.CalculatesService = CalculatesService = __decorate([
    (0, common_1.Injectable)()
], CalculatesService);
//# sourceMappingURL=calculates.service.js.map