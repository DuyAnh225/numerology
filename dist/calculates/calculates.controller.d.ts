import { CalculatesService } from "./calculates.service";
export declare class CalculatesController {
    private readonly calculatesService;
    constructor(calculatesService: CalculatesService);
    calculateLifePath(date: Date): Promise<number>;
    calculateMission(name: string): Promise<number>;
    calculateInternal(name: string): Promise<number>;
    calculatePersonality(name: string): Promise<number>;
    calculateConnection(name: string): Promise<number>;
    calculateBalance(name: string): Promise<number>;
    calculateMiss(name: string): Promise<number>;
    calculateRepeat(name: string): Promise<number>;
    calculateLogic(name: string): Promise<number>;
    calculateIntuitive(name: string): Promise<number>;
    calculateEmotional(name: string): Promise<number>;
    calculateExperience(name: string): Promise<number>;
    calculateBirthday(day: number): Promise<number>;
    calculateAttitude(day: number, month: number): Promise<number>;
    calculatePeak(date: Date): Promise<any>;
    calculateMaturity(date: Date, name: string): Promise<any>;
}
